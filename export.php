<?php
/*
 export.php
 Export data from databse to csv file
*/

require_once 'db.php';

/*
 Export data from database to fname, hdr_name is header file
*/
function export_csv($fname,$hdr_name)
{
	global $mysqli;
	$result = $mysqli->query("SELECT * FROM entries");
	$fp=fopen($fname,'w');
	echo "<pre>";
	$prop_list=array();
	while ($entry = $result->fetch_assoc()) 
    {
    	foreach ($prop_list as $key=>$value) 
    	{
    		$prop_list[$key]="";
    	}
    	print_r($entry);  
        $entry_id=$entry['id'];
        $attributes = $mysqli->query("SELECT * FROM attributes WHERE entry_id='$entry_id'");
        while ($attr = $attributes->fetch_assoc())
        {
        	$prop_list[$attr['name']]=$attr['value'];
        }
        print_r($prop_list); 
        $arr = array_merge($entry, $prop_list);
        fputcsv($fp, $arr, ';'); 
    }
    echo "</pre>";
    fclose($fp);
    $fp=fopen($hdr_name,'w');
    foreach($arr as $p=>$field) 
	{
		$hdr[] = $p;		
	}
	fputcsv($fp, $hdr, ';');
    fclose($fp);
}

export_csv("test.csv","header.csv");
echo "complited <br>\n";
?>