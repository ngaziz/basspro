<?php
/*
 repricer.php
 Read sku from text file, search pages with the same sku's and refresh prices in database
*/

ini_set('max_execution_time', 0);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'http_load.php';
require_once 'phpQuery-onefile.php';
require_once 'db.php';

/*
 Callback function for repricer. Function calls when webpage is loaded and search data for update
*/
function search_data($url, $content, $curl_status, $ch) 
{
    global $mysqli;
    global $sku_list;
    //$mysqli->query("UPDATE urls SET crawler='1' WHERE url='$url'");
    echo "\n Upload [$url] ";//.$content;
    if ( !$curl_status ) 
    {
        echo "success <br>\n";
        $dom = new DomDocument();
        @$dom->loadHTML( $content );
        $xpath = new DomXPath( $dom );

        $title = $xpath->query("//div[@class='title']")->item(0)->nodeValue;
        $title=str_replace(array("\n","\t"),"",$title);
        $title=trim($title);
        if ($title=="") 
        {
            $err = "Title not found";
            echo $err."<br>\n";
            //$mysqli->query("UPDATE urls SET status='-1' WHERE url='$url'");
            //$mysqli->query("UPDATE urls SET log='$err' WHERE url='$url'");
            return;
        }

        $elements = $xpath->query("//a[@class='breadcrumb brand name']");
        foreach ($elements as $element)
        {
            $cats_arr[] = $element->nodeValue;
        }
        $category = implode(" > ", $cats_arr);

        $description = $xpath->query("//div[@id='description']")->item(0)->nodeValue;
        $description=str_replace(array("\n","\t",'"'),"",$description);
        $description=trim($description);

        if (preg_match_all("/sku_[0-9]*.price.=.'([0-9]*.[0-9]*)'/i", $content, $arr)) $price = $arr[1];
        if (preg_match_all("/sku_[0-9]*.partNumber.=.'([0-9]*.[0-9]*)'/i", $content, $arr)) $sku = $arr[1];
        if (preg_match_all("/sku_[0-9]*.auxDesc1.=.'(.*)'/i", $content, $arr)) $stock = $arr[1];

        $arr = explode("/", $url);
        $id=$arr[count($arr)-2];
        foreach ($sku as $s) 
        {
            $image[]="https://basspro.scene7.com/is/image/BassPro/".$s."_".$id;        
        }        

        $elements = $xpath->query("//table[@id='chart']/tbody/tr/td[@class='quantity']/input");
        foreach ($elements as $element) 
        {
            $quantity[] = $element->getAttribute('value');
        }

        $results = phpQuery::newDocument($content);
        $elements = $results->find("table#chart > tbody > tr");        
        $cnt=0;
        foreach ($elements as $el) 
        {
            $rows = pq($el)->find("td > dl");
            foreach ($rows as $row) 
            {             
                $s = pq($row)->find("dt")->text();
                $s = str_replace(":", "", $s);
                $keys[$cnt][]=$s;
                $values[$cnt][] = pq($row)->find("dd")->text();
            }
            $cnt++;
        }    

        for ($i=0;$i<$cnt;$i++)
        {
            $entrys[$i]['url']=$url;
            $entrys[$i]['title']=$title;            
            $entrys[$i]['category']=$category;            
            $entrys[$i]['description']=$description;                        
            $entrys[$i]['sku']=$sku[$i];
            $entrys[$i]['price']=$price[$i];
            $entrys[$i]['stock']=$stock[$i];
            $entrys[$i]['image']=$image[$i];
            $entrys[$i]['quantity']=$quantity[$i];
            $price_=$price[$i];
            $sku_=trim($sku[$i]);
            $image_=$image[$i];
            $stock_=strip_tags($stock[$i]);
            $quantity_=$quantity[$i];
            $title=addslashes($title);
            $category=addslashes($category);
            $description=addslashes($description);
            $image_=addslashes($image_);

            $query = "SELECT * FROM entries WHERE sku='$sku_'";
            $statement = $mysqli->prepare($query);
            if (!$statement)
            {
                echo "MySQL error: ".$mysqli->error;
            }
            $statement->execute();                           
            $result=$statement->get_result();          
            $num_rows=$result->num_rows;            
            $statement->close();

            if (($num_rows!=0)&&(in_array($sku_, $sku_list)))
            {                
                $query = "UPDATE entries SET price='$price_' WHERE sku='$sku_'";
                $statement = $mysqli->prepare($query);
                if (!$statement)
                {
                    echo "MySQL error: ".$mysqli->error;
                }
                else
                {
                    $statement->execute();                           
                    $statement->close();
                    echo "Updated SKU ".$sku_." new price is ".$price_."<br>\n";
                }
            }
        }
        //print_r($entrys);        
                    
        /*$mysqli->query("UPDATE urls SET status='1' WHERE url='$url'");
        $mysqli->query("UPDATE urls SET log='ok' WHERE url='$url'");*/
    }
    else {  
        $err = curl_error( $ch );
        echo "error #$curl_status: ".$err."<br>\n";
        //$mysqli->query("UPDATE urls SET status='-1' WHERE url='$url'");
        //$mysqli->query("UPDATE urls SET log='$err' WHERE url='$url'");
    }
}

/*
 Read SKUs from text file
*/
function read_sku($fname)
{
    $fp=fopen($fname,"r");
    while (!feof($fp))
    {
        $s=fgets($fp);
        $s=str_replace(array("\n","\r"), "", $s);
        $sku[]=$s;
    }
    fclose($fp);
    return $sku;
}

/*
 Main function for repricer. Read SKUs from text file, search URLs with the same SKUs and update it
*/
function run_repricer()
{
    global $mysqli;
    global $max_threads;
    global $sku_list;
    $sku_list = read_sku("sku.txt");
    $s=join(',',$sku_list);    
    $query = "SELECT * FROM entries WHERE sku IN ($s)";         
    if (!$statement = $mysqli->prepare($query))
    {
        echo "MySQL error: ".$mysqli->error;
    }
    $statement->execute();                           
    $result=$statement->get_result();          
    $num_rows=$statement->num_rows;              
    $statement->close();
    while ($row = $result->fetch_assoc()) 
    {
        $urls[]=$row['url'];
    }    
    //print_r($urls);

    $count=0;
    while ($count<count($urls))
    {
      $run_urls=array();
      for ($i=0;$i<$max_threads;$i++)
      {
        $run_urls[$i]=$urls[$count];
        $count++;
        if ($count>=count($urls)) break;
      }
      @var_export(http_load($run_urls, search_data));
      sleep(1);
  }
}

echo "<pre>";
$max_threads=2;
$sku_list=array();
run_repricer();
echo "</pre>";
echo "complited <br>\n";
?> 