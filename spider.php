<?php
/*
 spider.php
 Collect all URLs from website and write it in table "urls" in database
*/

ini_set('max_execution_time', 0);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'http_load.php';
require_once 'db.php';

/*
 Search all inner links on webpage and write it in database
*/
function inlinks($url, $content)
{
	global $base_url;
	global $mysqli;
	preg_match_all ( '~<a href="(.*?)".*?>(.*?)</a>~si', $content, $links );	
	$count=0;
	for ($i=0; $i<count($links[0]); $i++)
	{
		if (substr_count($links[1][$i],'//')==0)
		{
  			$link = $base_url.$links[1][$i];
  			//echo $link."<br>";
  			if (substr_count($links[1][$i],'/product/')!=0)
  			{
  				if ($mysqli->query("INSERT INTO urls (url,spider,crawler,status,log) VALUES ('$link','0','0','0','')")) $count++;
  			}
  			else
  			{
  				if ($mysqli->query("INSERT INTO urls (url,spider,crawler,status,log) VALUES ('$link','0','1','0','')")) $count++;
  			}
  		}
 	}
 	echo $count." urls added <br>\n";
 	return $count;
}

/*
 Callback function for spider. Function calls when webpage is loaded
*/
function search_links( $url, $content, $curl_status, $ch) 
{
	global $mysqli;
	$mysqli->query("UPDATE urls SET spider='1' WHERE url='$url'");
	echo "Upload [$url] ";
	if ( !$curl_status ) 
	{
		echo "success <br>\n";
		inlinks($url,$content);
		run_spider();
	}
	else
	{
		echo "error #$curl_status: ".curl_error( $ch )."<br>\n";
	}
}

/*
 Main spider fuction. Read URLs from database and loading webpages for search new URLs
*/
function run_spider()
{
	global $mysqli;
	global $max_threads;
	$num=0;
	$query = "SELECT * FROM urls WHERE spider='0' LIMIT 100";
	$statement = $mysqli->prepare($query);
	if (!$statement)
	{
		echo "MySQL error: ".$mysqli->error;
		return;
	}
	$statement->execute();                           
    $result=$statement->get_result();                  
	$statement->close();

	$urls=array();
	while ($row = $result->fetch_assoc()) 
	{
  		$urls[]=$row['url'];
  		$num++;
	}
	$count=0;
	//print_r($urls);
	while ($count<count($urls))
	{
		$run_urls=array();
		for ($i=0;$i<$max_threads;$i++)
		{
			$run_urls[$i]=$urls[$count];
			$count++;
			if ($count>=count($urls)) break;
		}
		//print_r($run_urls);
		@var_export( http_load( $run_urls, search_links ) );
		sleep(1);
	}
	return $num;
}

echo "<pre>";
$max_threads=2;	//max threads count for loading webpages
$num=1;
while ($num>0)	//run spider in cycle while a new URLs is founded
{
    $num=run_spider();
}
echo "complited <br>\n";
?>