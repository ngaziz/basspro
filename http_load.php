<?php
/*
 http_load.php
 Load pages from website with multi threading
*/

function http_load($urls, $callback = false ) 
{
    $mh = curl_multi_init();      
    $chs = array();
    foreach ( $urls as $url ) 
    {
        $chs[] = ( $ch = curl_init() );
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible)');
        curl_setopt($ch, CURLOPT_HEADER, 0 );
        curl_setopt($ch, CURLOPT_COOKIEJAR, "cookies.txt"); // cookies storage / here the changes have been made
        curl_setopt($ch, CURLOPT_COOKIEFILE, "cookies.txt");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30 );
        curl_setopt($ch, CURLOPT_TIMEOUT, 30 );
        curl_multi_add_handle( $mh, $ch );
    }       
    // if $callback is false, then not call $callback and return results
    if ( $callback === false ) 
    {
        $results = array();
    }      
    $prev_running = $running = null;
    do 
    {
        curl_multi_exec( $mh, $running );        
        if ( $running != $prev_running ) 
        {
            // get info about current connections
            $info = curl_multi_info_read( $mh );          
            if ( is_array( $info ) && ( $ch = $info['handle'] ) ) 
            {
                // get loaded pages content
                $content = curl_multi_getcontent( $ch );                  
                // get loaded url
                $url = curl_getinfo( $ch, CURLINFO_EFFECTIVE_URL );                 
                if ( $callback !== false ) 
                {
                    // call callback function
                    $callback( $url, $content, $info['result'], $ch );
                }
                else 
                {
                    // adding results to hash
                    $results[ $url ] = array( 'content' => $content, 'status' => $info['result'], 'status_text' => curl_error( $ch ) );
                }               
            }             
            // update hashed active connections counter
            $prev_running = $running;
        }           
    } while ( $running > 0 );      
    foreach ( $chs as $ch ) 
    {
        curl_multi_remove_handle( $mh, $ch );
        curl_close( $ch );
    }
    curl_multi_close( $mh );      
    // results
    return ( $callback !== false ) ? true : $results;
}
?>