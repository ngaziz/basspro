<?php
/*
 crawler.php
 Read URLs form table 'urls' in database, scrap each URL and write founded data in table 'entries', 
 founded attributes write in table 'attributes'
*/

ini_set('max_execution_time', 0);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'http_load.php';
require_once 'phpQuery-onefile.php';
require_once 'db.php';

/*
 Callback function for crawler. Function calls when webpage is loaded and scrap all data from it.
*/
function search_data( $url, $content, $curl_status, $ch) 
{
    global $mysqli;
    $mysqli->query("UPDATE urls SET crawler='1' WHERE url='$url'");
    echo "\n Upload [$url] ";//.$content;
    if ( !$curl_status ) 
    {
        echo "success <br>\n";
        $dom = new DomDocument();
        @$dom->loadHTML( $content );
        $xpath = new DomXPath( $dom );

        $title = $xpath->query("//div[@class='title']")->item(0)->nodeValue;
        $title=str_replace(array("\n","\t"),"",$title);
        $title=trim($title);
        if ($title=="") 
        {
            $err = "Title not found";
            echo $err."<br>\n";
            $mysqli->query("UPDATE urls SET status='-1' WHERE url='$url'");
            $mysqli->query("UPDATE urls SET log='$err' WHERE url='$url'");
            return;
        }

        $elements = $xpath->query("//a[@class='breadcrumb brand name']");
        foreach ($elements as $element)
        {
            $cats_arr[] = $element->nodeValue;
        }
        $category = implode(" > ", $cats_arr);

        $description = $xpath->query("//div[@id='description']")->item(0)->nodeValue;
        $description=str_replace(array("\n","\t",'"'),"",$description);
        $description=trim($description);

        if (preg_match_all("/sku_[0-9]*.price.=.'([0-9]*.[0-9]*)'/i", $content, $arr)) $price = $arr[1];
        if (preg_match_all("/sku_[0-9]*.partNumber.=.'([0-9]*.[0-9]*)'/i", $content, $arr)) $sku = $arr[1];
        if (preg_match_all("/sku_[0-9]*.auxDesc1.=.'(.*)'/i", $content, $arr)) $stock = $arr[1];

        $arr = explode("/", $url);
        $id=$arr[count($arr)-2];
        foreach ($sku as $s) 
        {
            $image[]="https://basspro.scene7.com/is/image/BassPro/".$s."_".$id;        
        }        

        $elements = $xpath->query("//table[@id='chart']/tbody/tr/td[@class='quantity']/input");
        foreach ($elements as $element) 
        {
            $quantity[] = $element->getAttribute('value');
        }

        $results = phpQuery::newDocument($content);
        $elements = $results->find("table#chart > tbody > tr");        
        $cnt=0;
        foreach ($elements as $el) 
        {
            $rows = pq($el)->find("td > dl");
            foreach ($rows as $row) 
            {             
                $s = pq($row)->find("dt")->text();
                $s = str_replace(":", "", $s);
                $keys[$cnt][]=$s;
                $values[$cnt][] = pq($row)->find("dd")->text();
            }
            $cnt++;
        }    

        for ($i=0;$i<$cnt;$i++)
        {
            $entrys[$i]['url']=$url;
            $entrys[$i]['title']=$title;            
            $entrys[$i]['category']=$category;            
            $entrys[$i]['description']=$description;                        
            $entrys[$i]['sku']=$sku[$i];
            $entrys[$i]['price']=$price[$i];
            $entrys[$i]['stock']=$stock[$i];
            $entrys[$i]['image']=$image[$i];
            $entrys[$i]['quantity']=$quantity[$i];
            $price_=$price[$i];
            $sku_=$sku[$i];
            $image_=$image[$i];
            $stock_=strip_tags($stock[$i]);
            $quantity_=$quantity[$i];
            $title=addcslashes($title);
            $category=addcslashes($category);
            $description=addcslashes($description);
            $image_=$image_;

            $query = "INSERT INTO entries (url, title, category, description, image, sku, price, quantity, stock) VALUES ('$url', '$title', '$category', '$description', '$image_', '$sku_', '$price_', '$quantity_', '$stock_')";
            $statement = $mysqli->prepare($query);
            if (!$statement)
            {
                echo "MySQL error: ".$mysqli->error;
            }
            $statement->execute();                           
            $statement->close();            
            $id=$mysqli->insert_id;
            //var_dump($id);
            for ($j=0;$j<=count($keys);$j++)
            {
                $attributes[$i][$keys[$i][$j]]=$values[$i][$j];
                $name = $keys[$i][$j];
                $value = $values[$i][$j];
                $query = "INSERT INTO attributes (entry_id, name, value) VALUES ('$id','$name','$value')";
                $statement = $mysqli->prepare($query);
                if (!$statement)
                {
                    echo "MySQL error: ".$mysqli->error;
                }
                $statement->execute();                           
                $statement->close();
            }
        }
        //print_r($entrys);
        //print_r($attributes);
                    
        $mysqli->query("UPDATE urls SET status='1' WHERE url='$url'");
        $mysqli->query("UPDATE urls SET log='ok' WHERE url='$url'");
    }
    else {  
        $err = curl_error( $ch );
        echo "error #$curl_status: ".$err."<br>\n";
        $mysqli->query("UPDATE urls SET status='-1' WHERE url='$url'");
        $mysqli->query("UPDATE urls SET log='$err' WHERE url='$url'");
    }
}

/*
 Main function for crawler. Read URLs from database and loading webpages for search data
*/
function run_crawler()
{
    global $mysqli;
    global $max_threads;
    $result = $mysqli->query("SELECT * FROM urls WHERE crawler='0' LIMIT 100");
    $urls=array();
    $num=0;
    while ($row = $result->fetch_assoc()) 
    {
        $urls[]=$row['url'];
        $num++;
    }
    //print_r($urls);
    $count=0;
    while ($count<count($urls))
    {
      $run_urls=array();
      for ($i=0;$i<$max_threads;$i++)
      {
        $run_urls[$i]=$urls[$count];
        $count++;
        if ($count>=count($urls)) break;
      }
      @var_export( http_load( $run_urls, search_data ) );
      sleep(1);
    }
    return $num;
}

$max_threads=10;    //max threads count for loading webpages
$num=1;
while ($num>0)
{
    $num=run_crawler(); //run crawler in cycle while a new URLs is founded
}
echo "complited <br>\n";
?> 