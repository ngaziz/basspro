Basspro scraper and repricer

Files:
clear_urls.bat			bat file for run clear_urls script
export.bat				bat file for run export script
run_crawler.bat			bat file for run crawler script
run_repricer.bat		bat file for run repricer script
run_spider.bat			bat file for run spider script
clear_urls.php			script for clear all URLs and database except base ULR
crawler.php				script for scrap data from founded URLs
db.php					script for connect to database
export.php				script for export data from database to csv file
http_load.php			script for loading webpages
phpQuery-onefile.php	phpQuery library for parse html document
repricer.php			script for update prices for products with set SKUs
spider.php				script for search URLs for scraping
sku.txt					text file with SKUs list for repricing
basspro.sql				mysql database damp with 3 tables 'urls', 'entries' and 'attributes'

For start a work with Basspro scraper you need:
1) Create 'basspro' database and import 3 tables from basspro.sql file in it.
2) Edit db.php file to set your host, user name and password.
3) Start run_spider.bat for collect URLs for scraping.
4) When URLs will be collected start run_crawler.bat for collect data from all URLs.
5) When data will be collected start export.bat for export data to csv file.
6) Write SKUs in sku.txt file and start repricer.bat for update prices.